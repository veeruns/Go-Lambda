Plugin  may be small script to do some basic host checking

*  A general package to run a set of plugins
* A plugin to check disk utilization is above a configurable limit and report OK,WARNING,CRITICAL
* A plugin to check cpu utilization over last 5 minutes in 1 minute interval
* A plugin to save metrics every minute AVG, MEDIA, 99%, 95% and 80% Confidence intervals
* A plugin to find diskio reports OK, WARNING, CRITICAL
* A plugin to find expiry, a configuration to when to warn

Go functions in Lamda

# Goals:

## Learn Golang

1. Running go code in Lambda
* Have Go code as the alexa's backend
* Have the go callback a server for some resource
* TLS server that implements mutual authentication
* Certificate authority with softhsm2
* Go-opencv integration

---
## Todo

1. Raspberry pi, opencv motion detector + AMAZON rekognition + Alexa
2. Raspberry Pi, plex media server + Alexa
* Golang certificate authority with integration to either opensc or softhsm2
